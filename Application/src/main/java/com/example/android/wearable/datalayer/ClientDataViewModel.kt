package com.example.android.wearable.datalayer

import android.graphics.Bitmap
import android.util.Log
import androidx.annotation.StringRes
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.google.android.gms.wearable.*
import com.google.gson.Gson

/**
 * A state holder for the client data.
 */
class ClientDataViewModel :
    ViewModel(),
    MessageClient.OnMessageReceivedListener{

    override fun onMessageReceived(messageEvent: MessageEvent) {
        Log.d("PhoneMessage",messageEvent.toString())
        val data = Gson().fromJson(messageEvent.data.decodeToString(),MyNode::class.java)
        Log.d("PhoneMessage",data.toString())

    }
}



data class MyNode(
    val node: String,
    val name: String
)