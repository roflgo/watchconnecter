package com.example.android.wearable.datalayer

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

/**
 * The UI affording the actions the user can take, along with a list of the events and the image
 * to be sent to the wearable devices.
 */
@Composable
fun MainApp(
    events: List<String> = emptyList()
) {
    LazyColumn(contentPadding = PaddingValues(16.dp)) {
        item {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(Modifier.weight(1f)) {
                    Button(
                        onClick = {  },
                        enabled = true
                    ) {
                        Text(stringResource(id = R.string.take_photo))
                    }
                    Button(
                        onClick = { } ,
                        enabled = true
                    ) {
                        Text(stringResource(id = R.string.send_photo))
                    }
                }

                Box(modifier = Modifier.size(100.dp)) {

                }
            }
            Divider()
        }
        item {
            Button(onClick = {  }) {
                Text(stringResource(id = R.string.start_wearable_activity))
            }
            Divider()
        }
        items(events) { event ->
            Column {
                Text(
                    stringResource(id = event.toInt()),
                    style = MaterialTheme.typography.subtitle1
                )
                Text(
                    event,
                    style = MaterialTheme.typography.body2
                )
            }
            Divider()
        }
    }
}
