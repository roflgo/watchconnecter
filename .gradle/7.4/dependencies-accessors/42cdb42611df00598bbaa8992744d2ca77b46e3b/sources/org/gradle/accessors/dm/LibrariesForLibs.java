package org.gradle.accessors.dm;

import org.gradle.api.NonNullApi;
import org.gradle.api.artifacts.MinimalExternalModuleDependency;
import org.gradle.plugin.use.PluginDependency;
import org.gradle.api.artifacts.ExternalModuleDependencyBundle;
import org.gradle.api.artifacts.MutableVersionConstraint;
import org.gradle.api.provider.Provider;
import org.gradle.api.provider.ProviderFactory;
import org.gradle.api.internal.catalog.AbstractExternalDependencyFactory;
import org.gradle.api.internal.catalog.DefaultVersionCatalog;
import java.util.Map;
import javax.inject.Inject;

/**
 * A catalog of dependencies accessible via the `libs` extension.
*/
@NonNullApi
public class LibrariesForLibs extends AbstractExternalDependencyFactory {

    private final AbstractExternalDependencyFactory owner = this;
    private final AndroidLibraryAccessors laccForAndroidLibraryAccessors = new AndroidLibraryAccessors(owner);
    private final AndroidxLibraryAccessors laccForAndroidxLibraryAccessors = new AndroidxLibraryAccessors(owner);
    private final ComLibraryAccessors laccForComLibraryAccessors = new ComLibraryAccessors(owner);
    private final ComposeLibraryAccessors laccForComposeLibraryAccessors = new ComposeLibraryAccessors(owner);
    private final JacocoLibraryAccessors laccForJacocoLibraryAccessors = new JacocoLibraryAccessors(owner);
    private final KotlinLibraryAccessors laccForKotlinLibraryAccessors = new KotlinLibraryAccessors(owner);
    private final KotlinxLibraryAccessors laccForKotlinxLibraryAccessors = new KotlinxLibraryAccessors(owner);
    private final OrgLibraryAccessors laccForOrgLibraryAccessors = new OrgLibraryAccessors(owner);
    private final PlayservicesLibraryAccessors laccForPlayservicesLibraryAccessors = new PlayservicesLibraryAccessors(owner);
    private final TestLibraryAccessors laccForTestLibraryAccessors = new TestLibraryAccessors(owner);
    private final WearLibraryAccessors laccForWearLibraryAccessors = new WearLibraryAccessors(owner);
    private final VersionAccessors vaccForVersionAccessors = new VersionAccessors(providers, config);
    private final BundleAccessors baccForBundleAccessors = new BundleAccessors(providers, config);
    private final PluginAccessors paccForPluginAccessors = new PluginAccessors(providers, config);

    @Inject
    public LibrariesForLibs(DefaultVersionCatalog config, ProviderFactory providers) {
        super(config, providers);
    }

        /**
         * Creates a dependency provider for junit (junit:junit)
         * This dependency was declared in catalog libs.versions.toml
         */
        public Provider<MinimalExternalModuleDependency> getJunit() { return create("junit"); }

    /**
     * Returns the group of libraries at android
     */
    public AndroidLibraryAccessors getAndroid() { return laccForAndroidLibraryAccessors; }

    /**
     * Returns the group of libraries at androidx
     */
    public AndroidxLibraryAccessors getAndroidx() { return laccForAndroidxLibraryAccessors; }

    /**
     * Returns the group of libraries at com
     */
    public ComLibraryAccessors getCom() { return laccForComLibraryAccessors; }

    /**
     * Returns the group of libraries at compose
     */
    public ComposeLibraryAccessors getCompose() { return laccForComposeLibraryAccessors; }

    /**
     * Returns the group of libraries at jacoco
     */
    public JacocoLibraryAccessors getJacoco() { return laccForJacocoLibraryAccessors; }

    /**
     * Returns the group of libraries at kotlin
     */
    public KotlinLibraryAccessors getKotlin() { return laccForKotlinLibraryAccessors; }

    /**
     * Returns the group of libraries at kotlinx
     */
    public KotlinxLibraryAccessors getKotlinx() { return laccForKotlinxLibraryAccessors; }

    /**
     * Returns the group of libraries at org
     */
    public OrgLibraryAccessors getOrg() { return laccForOrgLibraryAccessors; }

    /**
     * Returns the group of libraries at playservices
     */
    public PlayservicesLibraryAccessors getPlayservices() { return laccForPlayservicesLibraryAccessors; }

    /**
     * Returns the group of libraries at test
     */
    public TestLibraryAccessors getTest() { return laccForTestLibraryAccessors; }

    /**
     * Returns the group of libraries at wear
     */
    public WearLibraryAccessors getWear() { return laccForWearLibraryAccessors; }

    /**
     * Returns the group of versions at versions
     */
    public VersionAccessors getVersions() { return vaccForVersionAccessors; }

    /**
     * Returns the group of bundles at bundles
     */
    public BundleAccessors getBundles() { return baccForBundleAccessors; }

    /**
     * Returns the group of plugins at plugins
     */
    public PluginAccessors getPlugins() { return paccForPluginAccessors; }

    public static class AndroidLibraryAccessors extends SubDependencyFactory {
        private final AndroidBuildLibraryAccessors laccForAndroidBuildLibraryAccessors = new AndroidBuildLibraryAccessors(owner);
        private final AndroidLintLibraryAccessors laccForAndroidLintLibraryAccessors = new AndroidLintLibraryAccessors(owner);
        private final AndroidToolsLibraryAccessors laccForAndroidToolsLibraryAccessors = new AndroidToolsLibraryAccessors(owner);

        public AndroidLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for desugarjdklibs (com.android.tools:desugar_jdk_libs)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getDesugarjdklibs() { return create("android.desugarjdklibs"); }

            /**
             * Creates a dependency provider for material (com.google.android.material:material)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getMaterial() { return create("android.material"); }

        /**
         * Returns the group of libraries at android.build
         */
        public AndroidBuildLibraryAccessors getBuild() { return laccForAndroidBuildLibraryAccessors; }

        /**
         * Returns the group of libraries at android.lint
         */
        public AndroidLintLibraryAccessors getLint() { return laccForAndroidLintLibraryAccessors; }

        /**
         * Returns the group of libraries at android.tools
         */
        public AndroidToolsLibraryAccessors getTools() { return laccForAndroidToolsLibraryAccessors; }

    }

    public static class AndroidBuildLibraryAccessors extends SubDependencyFactory {

        public AndroidBuildLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for gradle (com.android.tools.build:gradle)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getGradle() { return create("android.build.gradle"); }

    }

    public static class AndroidLintLibraryAccessors extends SubDependencyFactory {

        public AndroidLintLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for gradle (com.android.tools.lint:lint-gradle)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getGradle() { return create("android.lint.gradle"); }

    }

    public static class AndroidToolsLibraryAccessors extends SubDependencyFactory {
        private final AndroidToolsLintLibraryAccessors laccForAndroidToolsLintLibraryAccessors = new AndroidToolsLintLibraryAccessors(owner);

        public AndroidToolsLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at android.tools.lint
         */
        public AndroidToolsLintLibraryAccessors getLint() { return laccForAndroidToolsLintLibraryAccessors; }

    }

    public static class AndroidToolsLintLibraryAccessors extends SubDependencyFactory {

        public AndroidToolsLintLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for gradle (com.android.tools.lint:lint-gradle)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getGradle() { return create("android.tools.lint.gradle"); }

    }

    public static class AndroidxLibraryAccessors extends SubDependencyFactory {
        private final AndroidxActivityLibraryAccessors laccForAndroidxActivityLibraryAccessors = new AndroidxActivityLibraryAccessors(owner);
        private final AndroidxConstraintlayoutLibraryAccessors laccForAndroidxConstraintlayoutLibraryAccessors = new AndroidxConstraintlayoutLibraryAccessors(owner);
        private final AndroidxCoreLibraryAccessors laccForAndroidxCoreLibraryAccessors = new AndroidxCoreLibraryAccessors(owner);
        private final AndroidxDatabindingLibraryAccessors laccForAndroidxDatabindingLibraryAccessors = new AndroidxDatabindingLibraryAccessors(owner);
        private final AndroidxDatastoreLibraryAccessors laccForAndroidxDatastoreLibraryAccessors = new AndroidxDatastoreLibraryAccessors(owner);
        private final AndroidxFragmentLibraryAccessors laccForAndroidxFragmentLibraryAccessors = new AndroidxFragmentLibraryAccessors(owner);
        private final AndroidxLifecycleLibraryAccessors laccForAndroidxLifecycleLibraryAccessors = new AndroidxLifecycleLibraryAccessors(owner);
        private final AndroidxWearLibraryAccessors laccForAndroidxWearLibraryAccessors = new AndroidxWearLibraryAccessors(owner);

        public AndroidxLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for appcompat (androidx.appcompat:appcompat)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getAppcompat() { return create("androidx.appcompat"); }

            /**
             * Creates a dependency provider for media (androidx.media:media)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getMedia() { return create("androidx.media"); }

            /**
             * Creates a dependency provider for window (androidx.window:window)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getWindow() { return create("androidx.window"); }

        /**
         * Returns the group of libraries at androidx.activity
         */
        public AndroidxActivityLibraryAccessors getActivity() { return laccForAndroidxActivityLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.constraintlayout
         */
        public AndroidxConstraintlayoutLibraryAccessors getConstraintlayout() { return laccForAndroidxConstraintlayoutLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.core
         */
        public AndroidxCoreLibraryAccessors getCore() { return laccForAndroidxCoreLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.databinding
         */
        public AndroidxDatabindingLibraryAccessors getDatabinding() { return laccForAndroidxDatabindingLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.datastore
         */
        public AndroidxDatastoreLibraryAccessors getDatastore() { return laccForAndroidxDatastoreLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.fragment
         */
        public AndroidxFragmentLibraryAccessors getFragment() { return laccForAndroidxFragmentLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.lifecycle
         */
        public AndroidxLifecycleLibraryAccessors getLifecycle() { return laccForAndroidxLifecycleLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.wear
         */
        public AndroidxWearLibraryAccessors getWear() { return laccForAndroidxWearLibraryAccessors; }

    }

    public static class AndroidxActivityLibraryAccessors extends SubDependencyFactory {

        public AndroidxActivityLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for compose (androidx.activity:activity-compose)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCompose() { return create("androidx.activity.compose"); }

            /**
             * Creates a dependency provider for ktx (androidx.activity:activity-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("androidx.activity.ktx"); }

    }

    public static class AndroidxConstraintlayoutLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public AndroidxConstraintlayoutLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for constraintlayout (androidx.constraintlayout:constraintlayout)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("androidx.constraintlayout"); }

            /**
             * Creates a dependency provider for compose (androidx.constraintlayout:constraintlayout-compose)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCompose() { return create("androidx.constraintlayout.compose"); }

    }

    public static class AndroidxCoreLibraryAccessors extends SubDependencyFactory {

        public AndroidxCoreLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ktx (androidx.core:core-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("androidx.core.ktx"); }

    }

    public static class AndroidxDatabindingLibraryAccessors extends SubDependencyFactory {

        public AndroidxDatabindingLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for viewbinding (androidx.databinding:viewbinding)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getViewbinding() { return create("androidx.databinding.viewbinding"); }

    }

    public static class AndroidxDatastoreLibraryAccessors extends SubDependencyFactory {

        public AndroidxDatastoreLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for preferences (androidx.datastore:datastore-preferences)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getPreferences() { return create("androidx.datastore.preferences"); }

    }

    public static class AndroidxFragmentLibraryAccessors extends SubDependencyFactory {

        public AndroidxFragmentLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ktx (androidx.fragment:fragment-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("androidx.fragment.ktx"); }

    }

    public static class AndroidxLifecycleLibraryAccessors extends SubDependencyFactory {
        private final AndroidxLifecycleCommonLibraryAccessors laccForAndroidxLifecycleCommonLibraryAccessors = new AndroidxLifecycleCommonLibraryAccessors(owner);
        private final AndroidxLifecycleRuntimeLibraryAccessors laccForAndroidxLifecycleRuntimeLibraryAccessors = new AndroidxLifecycleRuntimeLibraryAccessors(owner);
        private final AndroidxLifecycleViewmodelLibraryAccessors laccForAndroidxLifecycleViewmodelLibraryAccessors = new AndroidxLifecycleViewmodelLibraryAccessors(owner);

        public AndroidxLifecycleLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at androidx.lifecycle.common
         */
        public AndroidxLifecycleCommonLibraryAccessors getCommon() { return laccForAndroidxLifecycleCommonLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.lifecycle.runtime
         */
        public AndroidxLifecycleRuntimeLibraryAccessors getRuntime() { return laccForAndroidxLifecycleRuntimeLibraryAccessors; }

        /**
         * Returns the group of libraries at androidx.lifecycle.viewmodel
         */
        public AndroidxLifecycleViewmodelLibraryAccessors getViewmodel() { return laccForAndroidxLifecycleViewmodelLibraryAccessors; }

    }

    public static class AndroidxLifecycleCommonLibraryAccessors extends SubDependencyFactory {

        public AndroidxLifecycleCommonLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for java8 (androidx.lifecycle:lifecycle-common-java8)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getJava8() { return create("androidx.lifecycle.common.java8"); }

    }

    public static class AndroidxLifecycleRuntimeLibraryAccessors extends SubDependencyFactory {

        public AndroidxLifecycleRuntimeLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ktx (androidx.lifecycle:lifecycle-runtime-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("androidx.lifecycle.runtime.ktx"); }

    }

    public static class AndroidxLifecycleViewmodelLibraryAccessors extends SubDependencyFactory {

        public AndroidxLifecycleViewmodelLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for compose (androidx.lifecycle:lifecycle-viewmodel-compose)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCompose() { return create("androidx.lifecycle.viewmodel.compose"); }

            /**
             * Creates a dependency provider for ktx (androidx.lifecycle:lifecycle-viewmodel-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("androidx.lifecycle.viewmodel.ktx"); }

            /**
             * Creates a dependency provider for savedstate (androidx.lifecycle:lifecycle-viewmodel-savedstate)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getSavedstate() { return create("androidx.lifecycle.viewmodel.savedstate"); }

    }

    public static class AndroidxWearLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {
        private final AndroidxWearWatchfaceLibraryAccessors laccForAndroidxWearWatchfaceLibraryAccessors = new AndroidxWearWatchfaceLibraryAccessors(owner);

        public AndroidxWearLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for wear (androidx.wear:wear)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("androidx.wear"); }

        /**
         * Returns the group of libraries at androidx.wear.watchface
         */
        public AndroidxWearWatchfaceLibraryAccessors getWatchface() { return laccForAndroidxWearWatchfaceLibraryAccessors; }

    }

    public static class AndroidxWearWatchfaceLibraryAccessors extends SubDependencyFactory {
        private final AndroidxWearWatchfaceComplicationsLibraryAccessors laccForAndroidxWearWatchfaceComplicationsLibraryAccessors = new AndroidxWearWatchfaceComplicationsLibraryAccessors(owner);

        public AndroidxWearWatchfaceLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at androidx.wear.watchface.complications
         */
        public AndroidxWearWatchfaceComplicationsLibraryAccessors getComplications() { return laccForAndroidxWearWatchfaceComplicationsLibraryAccessors; }

    }

    public static class AndroidxWearWatchfaceComplicationsLibraryAccessors extends SubDependencyFactory {
        private final AndroidxWearWatchfaceComplicationsDataLibraryAccessors laccForAndroidxWearWatchfaceComplicationsDataLibraryAccessors = new AndroidxWearWatchfaceComplicationsDataLibraryAccessors(owner);

        public AndroidxWearWatchfaceComplicationsLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at androidx.wear.watchface.complications.data
         */
        public AndroidxWearWatchfaceComplicationsDataLibraryAccessors getData() { return laccForAndroidxWearWatchfaceComplicationsDataLibraryAccessors; }

    }

    public static class AndroidxWearWatchfaceComplicationsDataLibraryAccessors extends SubDependencyFactory {
        private final AndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors laccForAndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors = new AndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors(owner);

        public AndroidxWearWatchfaceComplicationsDataLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at androidx.wear.watchface.complications.data.source
         */
        public AndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors getSource() { return laccForAndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors; }

    }

    public static class AndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors extends SubDependencyFactory {

        public AndroidxWearWatchfaceComplicationsDataSourceLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ktx (androidx.wear.watchface:watchface-complications-data-source-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("androidx.wear.watchface.complications.data.source.ktx"); }

    }

    public static class ComLibraryAccessors extends SubDependencyFactory {
        private final ComGoogleLibraryAccessors laccForComGoogleLibraryAccessors = new ComGoogleLibraryAccessors(owner);

        public ComLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at com.google
         */
        public ComGoogleLibraryAccessors getGoogle() { return laccForComGoogleLibraryAccessors; }

    }

    public static class ComGoogleLibraryAccessors extends SubDependencyFactory {

        public ComGoogleLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for truth (com.google.truth:truth)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getTruth() { return create("com.google.truth"); }

    }

    public static class ComposeLibraryAccessors extends SubDependencyFactory {
        private final ComposeMaterialLibraryAccessors laccForComposeMaterialLibraryAccessors = new ComposeMaterialLibraryAccessors(owner);
        private final ComposeUiLibraryAccessors laccForComposeUiLibraryAccessors = new ComposeUiLibraryAccessors(owner);

        public ComposeLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for compiler (androidx.compose.compiler:compiler)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCompiler() { return create("compose.compiler"); }

            /**
             * Creates a dependency provider for foundation (androidx.compose.foundation:foundation)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getFoundation() { return create("compose.foundation"); }

        /**
         * Returns the group of libraries at compose.material
         */
        public ComposeMaterialLibraryAccessors getMaterial() { return laccForComposeMaterialLibraryAccessors; }

        /**
         * Returns the group of libraries at compose.ui
         */
        public ComposeUiLibraryAccessors getUi() { return laccForComposeUiLibraryAccessors; }

    }

    public static class ComposeMaterialLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {
        private final ComposeMaterialIconsLibraryAccessors laccForComposeMaterialIconsLibraryAccessors = new ComposeMaterialIconsLibraryAccessors(owner);

        public ComposeMaterialLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for material (androidx.compose.material:material)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("compose.material"); }

            /**
             * Creates a dependency provider for ripple (androidx.compose.material:material-ripple)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRipple() { return create("compose.material.ripple"); }

        /**
         * Returns the group of libraries at compose.material.icons
         */
        public ComposeMaterialIconsLibraryAccessors getIcons() { return laccForComposeMaterialIconsLibraryAccessors; }

    }

    public static class ComposeMaterialIconsLibraryAccessors extends SubDependencyFactory {

        public ComposeMaterialIconsLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for extended (androidx.compose.material:material-icons-extended)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getExtended() { return create("compose.material.icons.extended"); }

    }

    public static class ComposeUiLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {
        private final ComposeUiTestLibraryAccessors laccForComposeUiTestLibraryAccessors = new ComposeUiTestLibraryAccessors(owner);
        private final ComposeUiToolingLibraryAccessors laccForComposeUiToolingLibraryAccessors = new ComposeUiToolingLibraryAccessors(owner);

        public ComposeUiLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ui (androidx.compose.ui:ui)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("compose.ui"); }

        /**
         * Returns the group of libraries at compose.ui.test
         */
        public ComposeUiTestLibraryAccessors getTest() { return laccForComposeUiTestLibraryAccessors; }

        /**
         * Returns the group of libraries at compose.ui.tooling
         */
        public ComposeUiToolingLibraryAccessors getTooling() { return laccForComposeUiToolingLibraryAccessors; }

    }

    public static class ComposeUiTestLibraryAccessors extends SubDependencyFactory {

        public ComposeUiTestLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for junit4 (androidx.compose.ui:ui-test-junit4)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getJunit4() { return create("compose.ui.test.junit4"); }

            /**
             * Creates a dependency provider for manifest (androidx.compose.ui:ui-test-manifest)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getManifest() { return create("compose.ui.test.manifest"); }

    }

    public static class ComposeUiToolingLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public ComposeUiToolingLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for tooling (androidx.compose.ui:ui-tooling)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("compose.ui.tooling"); }

            /**
             * Creates a dependency provider for preview (androidx.compose.ui:ui-tooling-preview)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getPreview() { return create("compose.ui.tooling.preview"); }

    }

    public static class JacocoLibraryAccessors extends SubDependencyFactory {

        public JacocoLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for ant (org.jacoco:org.jacoco.ant)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getAnt() { return create("jacoco.ant"); }

    }

    public static class KotlinLibraryAccessors extends SubDependencyFactory {
        private final KotlinAnnotationLibraryAccessors laccForKotlinAnnotationLibraryAccessors = new KotlinAnnotationLibraryAccessors(owner);
        private final KotlinGradleLibraryAccessors laccForKotlinGradleLibraryAccessors = new KotlinGradleLibraryAccessors(owner);
        private final KotlinParcelizeLibraryAccessors laccForKotlinParcelizeLibraryAccessors = new KotlinParcelizeLibraryAccessors(owner);
        private final KotlinScriptingLibraryAccessors laccForKotlinScriptingLibraryAccessors = new KotlinScriptingLibraryAccessors(owner);
        private final KotlinStdlibLibraryAccessors laccForKotlinStdlibLibraryAccessors = new KotlinStdlibLibraryAccessors(owner);

        public KotlinLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at kotlin.annotation
         */
        public KotlinAnnotationLibraryAccessors getAnnotation() { return laccForKotlinAnnotationLibraryAccessors; }

        /**
         * Returns the group of libraries at kotlin.gradle
         */
        public KotlinGradleLibraryAccessors getGradle() { return laccForKotlinGradleLibraryAccessors; }

        /**
         * Returns the group of libraries at kotlin.parcelize
         */
        public KotlinParcelizeLibraryAccessors getParcelize() { return laccForKotlinParcelizeLibraryAccessors; }

        /**
         * Returns the group of libraries at kotlin.scripting
         */
        public KotlinScriptingLibraryAccessors getScripting() { return laccForKotlinScriptingLibraryAccessors; }

        /**
         * Returns the group of libraries at kotlin.stdlib
         */
        public KotlinStdlibLibraryAccessors getStdlib() { return laccForKotlinStdlibLibraryAccessors; }

    }

    public static class KotlinAnnotationLibraryAccessors extends SubDependencyFactory {
        private final KotlinAnnotationProcessingLibraryAccessors laccForKotlinAnnotationProcessingLibraryAccessors = new KotlinAnnotationProcessingLibraryAccessors(owner);

        public KotlinAnnotationLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at kotlin.annotation.processing
         */
        public KotlinAnnotationProcessingLibraryAccessors getProcessing() { return laccForKotlinAnnotationProcessingLibraryAccessors; }

    }

    public static class KotlinAnnotationProcessingLibraryAccessors extends SubDependencyFactory {

        public KotlinAnnotationProcessingLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for gradle (org.jetbrains.kotlin:kotlin-annotation-processing-gradle)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getGradle() { return create("kotlin.annotation.processing.gradle"); }

    }

    public static class KotlinGradleLibraryAccessors extends SubDependencyFactory {

        public KotlinGradleLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for plugin (org.jetbrains.kotlin:kotlin-gradle-plugin)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getPlugin() { return create("kotlin.gradle.plugin"); }

    }

    public static class KotlinParcelizeLibraryAccessors extends SubDependencyFactory {

        public KotlinParcelizeLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for compiler (org.jetbrains.kotlin:kotlin-parcelize-compiler)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCompiler() { return create("kotlin.parcelize.compiler"); }

            /**
             * Creates a dependency provider for runtime (org.jetbrains.kotlin:kotlin-parcelize-runtime)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRuntime() { return create("kotlin.parcelize.runtime"); }

    }

    public static class KotlinScriptingLibraryAccessors extends SubDependencyFactory {
        private final KotlinScriptingCompilerLibraryAccessors laccForKotlinScriptingCompilerLibraryAccessors = new KotlinScriptingCompilerLibraryAccessors(owner);

        public KotlinScriptingLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at kotlin.scripting.compiler
         */
        public KotlinScriptingCompilerLibraryAccessors getCompiler() { return laccForKotlinScriptingCompilerLibraryAccessors; }

    }

    public static class KotlinScriptingCompilerLibraryAccessors extends SubDependencyFactory {

        public KotlinScriptingCompilerLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for embeddable (org.jetbrains.kotlin:kotlin-scripting-compiler-embeddable)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getEmbeddable() { return create("kotlin.scripting.compiler.embeddable"); }

    }

    public static class KotlinStdlibLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public KotlinStdlibLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for stdlib (org.jetbrains.kotlin:kotlin-stdlib)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("kotlin.stdlib"); }

            /**
             * Creates a dependency provider for jdk8 (org.jetbrains.kotlin:kotlin-stdlib-jdk8)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getJdk8() { return create("kotlin.stdlib.jdk8"); }

    }

    public static class KotlinxLibraryAccessors extends SubDependencyFactory {
        private final KotlinxCoroutinesLibraryAccessors laccForKotlinxCoroutinesLibraryAccessors = new KotlinxCoroutinesLibraryAccessors(owner);

        public KotlinxLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at kotlinx.coroutines
         */
        public KotlinxCoroutinesLibraryAccessors getCoroutines() { return laccForKotlinxCoroutinesLibraryAccessors; }

    }

    public static class KotlinxCoroutinesLibraryAccessors extends SubDependencyFactory {
        private final KotlinxCoroutinesPlayLibraryAccessors laccForKotlinxCoroutinesPlayLibraryAccessors = new KotlinxCoroutinesPlayLibraryAccessors(owner);

        public KotlinxCoroutinesLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for android (org.jetbrains.kotlinx:kotlinx-coroutines-android)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getAndroid() { return create("kotlinx.coroutines.android"); }

            /**
             * Creates a dependency provider for core (org.jetbrains.kotlinx:kotlinx-coroutines-core)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCore() { return create("kotlinx.coroutines.core"); }

            /**
             * Creates a dependency provider for guava (org.jetbrains.kotlinx:kotlinx-coroutines-guava)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getGuava() { return create("kotlinx.coroutines.guava"); }

            /**
             * Creates a dependency provider for test (org.jetbrains.kotlinx:kotlinx-coroutines-test)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getTest() { return create("kotlinx.coroutines.test"); }

        /**
         * Returns the group of libraries at kotlinx.coroutines.play
         */
        public KotlinxCoroutinesPlayLibraryAccessors getPlay() { return laccForKotlinxCoroutinesPlayLibraryAccessors; }

    }

    public static class KotlinxCoroutinesPlayLibraryAccessors extends SubDependencyFactory {

        public KotlinxCoroutinesPlayLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for services (org.jetbrains.kotlinx:kotlinx-coroutines-play-services)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getServices() { return create("kotlinx.coroutines.play.services"); }

    }

    public static class OrgLibraryAccessors extends SubDependencyFactory {

        public OrgLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for robolectric (org.robolectric:robolectric)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRobolectric() { return create("org.robolectric"); }

    }

    public static class PlayservicesLibraryAccessors extends SubDependencyFactory {

        public PlayservicesLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for auth (com.google.android.gms:play-services-auth)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getAuth() { return create("playservices.auth"); }

            /**
             * Creates a dependency provider for wearable (com.google.android.gms:play-services-wearable)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getWearable() { return create("playservices.wearable"); }

    }

    public static class TestLibraryAccessors extends SubDependencyFactory {
        private final TestCoreLibraryAccessors laccForTestCoreLibraryAccessors = new TestCoreLibraryAccessors(owner);
        private final TestEspressoLibraryAccessors laccForTestEspressoLibraryAccessors = new TestEspressoLibraryAccessors(owner);
        private final TestExtLibraryAccessors laccForTestExtLibraryAccessors = new TestExtLibraryAccessors(owner);

        public TestLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for rules (androidx.test:rules)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRules() { return create("test.rules"); }

            /**
             * Creates a dependency provider for runner (androidx.test:runner)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRunner() { return create("test.runner"); }

            /**
             * Creates a dependency provider for uiautomator (androidx.test.uiautomator:uiautomator)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getUiautomator() { return create("test.uiautomator"); }

        /**
         * Returns the group of libraries at test.core
         */
        public TestCoreLibraryAccessors getCore() { return laccForTestCoreLibraryAccessors; }

        /**
         * Returns the group of libraries at test.espresso
         */
        public TestEspressoLibraryAccessors getEspresso() { return laccForTestEspressoLibraryAccessors; }

        /**
         * Returns the group of libraries at test.ext
         */
        public TestExtLibraryAccessors getExt() { return laccForTestExtLibraryAccessors; }

    }

    public static class TestCoreLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public TestCoreLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for core (androidx.test:core)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("test.core"); }

            /**
             * Creates a dependency provider for ktx (androidx.test:core-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("test.core.ktx"); }

    }

    public static class TestEspressoLibraryAccessors extends SubDependencyFactory {

        public TestEspressoLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for core (androidx.test.espresso:espresso-core)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getCore() { return create("test.espresso.core"); }

    }

    public static class TestExtLibraryAccessors extends SubDependencyFactory {
        private final TestExtJunitLibraryAccessors laccForTestExtJunitLibraryAccessors = new TestExtJunitLibraryAccessors(owner);

        public TestExtLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for truth (androidx.test.ext:truth)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getTruth() { return create("test.ext.truth"); }

        /**
         * Returns the group of libraries at test.ext.junit
         */
        public TestExtJunitLibraryAccessors getJunit() { return laccForTestExtJunitLibraryAccessors; }

    }

    public static class TestExtJunitLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public TestExtJunitLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for junit (androidx.test.ext:junit)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("test.ext.junit"); }

            /**
             * Creates a dependency provider for ktx (androidx.test.ext:junit-ktx)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getKtx() { return create("test.ext.junit.ktx"); }

    }

    public static class WearLibraryAccessors extends SubDependencyFactory {
        private final WearComposeLibraryAccessors laccForWearComposeLibraryAccessors = new WearComposeLibraryAccessors(owner);
        private final WearPhoneLibraryAccessors laccForWearPhoneLibraryAccessors = new WearPhoneLibraryAccessors(owner);
        private final WearRemoteLibraryAccessors laccForWearRemoteLibraryAccessors = new WearRemoteLibraryAccessors(owner);
        private final WearTilesLibraryAccessors laccForWearTilesLibraryAccessors = new WearTilesLibraryAccessors(owner);
        private final WearWatchfaceLibraryAccessors laccForWearWatchfaceLibraryAccessors = new WearWatchfaceLibraryAccessors(owner);

        public WearLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

        /**
         * Returns the group of libraries at wear.compose
         */
        public WearComposeLibraryAccessors getCompose() { return laccForWearComposeLibraryAccessors; }

        /**
         * Returns the group of libraries at wear.phone
         */
        public WearPhoneLibraryAccessors getPhone() { return laccForWearPhoneLibraryAccessors; }

        /**
         * Returns the group of libraries at wear.remote
         */
        public WearRemoteLibraryAccessors getRemote() { return laccForWearRemoteLibraryAccessors; }

        /**
         * Returns the group of libraries at wear.tiles
         */
        public WearTilesLibraryAccessors getTiles() { return laccForWearTilesLibraryAccessors; }

        /**
         * Returns the group of libraries at wear.watchface
         */
        public WearWatchfaceLibraryAccessors getWatchface() { return laccForWearWatchfaceLibraryAccessors; }

    }

    public static class WearComposeLibraryAccessors extends SubDependencyFactory {

        public WearComposeLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for foundation (androidx.wear.compose:compose-foundation)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getFoundation() { return create("wear.compose.foundation"); }

            /**
             * Creates a dependency provider for material (androidx.wear.compose:compose-material)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getMaterial() { return create("wear.compose.material"); }

            /**
             * Creates a dependency provider for navigation (androidx.wear.compose:compose-navigation)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getNavigation() { return create("wear.compose.navigation"); }

    }

    public static class WearPhoneLibraryAccessors extends SubDependencyFactory {

        public WearPhoneLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for interactions (androidx.wear:wear-phone-interactions)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getInteractions() { return create("wear.phone.interactions"); }

    }

    public static class WearRemoteLibraryAccessors extends SubDependencyFactory {

        public WearRemoteLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for interactions (androidx.wear:wear-remote-interactions)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getInteractions() { return create("wear.remote.interactions"); }

    }

    public static class WearTilesLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public WearTilesLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for tiles (androidx.wear.tiles:tiles)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("wear.tiles"); }

            /**
             * Creates a dependency provider for renderer (androidx.wear.tiles:tiles-renderer)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRenderer() { return create("wear.tiles.renderer"); }

    }

    public static class WearWatchfaceLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {
        private final WearWatchfaceComplicationsLibraryAccessors laccForWearWatchfaceComplicationsLibraryAccessors = new WearWatchfaceComplicationsLibraryAccessors(owner);

        public WearWatchfaceLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for watchface (androidx.wear.watchface:watchface)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("wear.watchface"); }

            /**
             * Creates a dependency provider for client (androidx.wear.watchface:watchface-client)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getClient() { return create("wear.watchface.client"); }

            /**
             * Creates a dependency provider for data (androidx.wear.watchface:watchface-data)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getData() { return create("wear.watchface.data"); }

            /**
             * Creates a dependency provider for editor (androidx.wear.watchface:watchface-editor)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getEditor() { return create("wear.watchface.editor"); }

            /**
             * Creates a dependency provider for style (androidx.wear.watchface:watchface-style)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getStyle() { return create("wear.watchface.style"); }

        /**
         * Returns the group of libraries at wear.watchface.complications
         */
        public WearWatchfaceComplicationsLibraryAccessors getComplications() { return laccForWearWatchfaceComplicationsLibraryAccessors; }

    }

    public static class WearWatchfaceComplicationsLibraryAccessors extends SubDependencyFactory {
        private final WearWatchfaceComplicationsDataLibraryAccessors laccForWearWatchfaceComplicationsDataLibraryAccessors = new WearWatchfaceComplicationsDataLibraryAccessors(owner);

        public WearWatchfaceComplicationsLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for rendering (androidx.wear.watchface:watchface-complications-rendering)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getRendering() { return create("wear.watchface.complications.rendering"); }

        /**
         * Returns the group of libraries at wear.watchface.complications.data
         */
        public WearWatchfaceComplicationsDataLibraryAccessors getData() { return laccForWearWatchfaceComplicationsDataLibraryAccessors; }

    }

    public static class WearWatchfaceComplicationsDataLibraryAccessors extends SubDependencyFactory implements DependencyNotationSupplier {

        public WearWatchfaceComplicationsDataLibraryAccessors(AbstractExternalDependencyFactory owner) { super(owner); }

            /**
             * Creates a dependency provider for data (androidx.wear.watchface:watchface-complications-data)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> asProvider() { return create("wear.watchface.complications.data"); }

            /**
             * Creates a dependency provider for source (androidx.wear.watchface:watchface-complications-data-source)
             * This dependency was declared in catalog libs.versions.toml
             */
            public Provider<MinimalExternalModuleDependency> getSource() { return create("wear.watchface.complications.data.source"); }

    }

    public static class VersionAccessors extends VersionFactory  {

        private final AndroidxVersionAccessors vaccForAndroidxVersionAccessors = new AndroidxVersionAccessors(providers, config);
        private final OrgVersionAccessors vaccForOrgVersionAccessors = new OrgVersionAccessors(providers, config);
        public VersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: compose (1.2.0-alpha04)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getCompose() { return getVersion("compose"); }

            /**
             * Returns the version associated to this alias: ktlint (0.41.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getKtlint() { return getVersion("ktlint"); }

        /**
         * Returns the group of versions at versions.androidx
         */
        public AndroidxVersionAccessors getAndroidx() { return vaccForAndroidxVersionAccessors; }

        /**
         * Returns the group of versions at versions.org
         */
        public OrgVersionAccessors getOrg() { return vaccForOrgVersionAccessors; }

    }

    public static class AndroidxVersionAccessors extends VersionFactory  {

        private final AndroidxWearVersionAccessors vaccForAndroidxWearVersionAccessors = new AndroidxWearVersionAccessors(providers, config);
        public AndroidxVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: androidx.activity (1.4.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getActivity() { return getVersion("androidx.activity"); }

            /**
             * Returns the version associated to this alias: androidx.lifecycle (2.4.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getLifecycle() { return getVersion("androidx.lifecycle"); }

            /**
             * Returns the version associated to this alias: androidx.test (1.4.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getTest() { return getVersion("androidx.test"); }

        /**
         * Returns the group of versions at versions.androidx.wear
         */
        public AndroidxWearVersionAccessors getWear() { return vaccForAndroidxWearVersionAccessors; }

    }

    public static class AndroidxWearVersionAccessors extends VersionFactory  {

        public AndroidxWearVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: androidx.wear.compose (1.0.0-alpha17)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getCompose() { return getVersion("androidx.wear.compose"); }

            /**
             * Returns the version associated to this alias: androidx.wear.tiles (1.0.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getTiles() { return getVersion("androidx.wear.tiles"); }

            /**
             * Returns the version associated to this alias: androidx.wear.watchface (1.0.1)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getWatchface() { return getVersion("androidx.wear.watchface"); }

    }

    public static class OrgVersionAccessors extends VersionFactory  {

        private final OrgJetbrainsVersionAccessors vaccForOrgJetbrainsVersionAccessors = new OrgJetbrainsVersionAccessors(providers, config);
        public OrgVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of versions at versions.org.jetbrains
         */
        public OrgJetbrainsVersionAccessors getJetbrains() { return vaccForOrgJetbrainsVersionAccessors; }

    }

    public static class OrgJetbrainsVersionAccessors extends VersionFactory  {

        public OrgJetbrainsVersionAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Returns the version associated to this alias: org.jetbrains.kotlin (1.6.10)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getKotlin() { return getVersion("org.jetbrains.kotlin"); }

            /**
             * Returns the version associated to this alias: org.jetbrains.kotlinx (1.6.0)
             * If the version is a rich version and that its not expressible as a
             * single version string, then an empty string is returned.
             * This version was declared in catalog libs.versions.toml
             */
            public Provider<String> getKotlinx() { return getVersion("org.jetbrains.kotlinx"); }

    }

    public static class BundleAccessors extends BundleFactory {

        public BundleAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

    }

    public static class PluginAccessors extends PluginFactory {
        private final AndroidPluginAccessors baccForAndroidPluginAccessors = new AndroidPluginAccessors(providers, config);
        private final ComPluginAccessors baccForComPluginAccessors = new ComPluginAccessors(providers, config);
        private final KotlinPluginAccessors baccForKotlinPluginAccessors = new KotlinPluginAccessors(providers, config);
        private final OrgPluginAccessors baccForOrgPluginAccessors = new OrgPluginAccessors(providers, config);

        public PluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for kotlin2js to the plugin id 'kotlin2js'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getKotlin2js() { return createPlugin("kotlin2js"); }

        /**
         * Returns the group of bundles at plugins.android
         */
        public AndroidPluginAccessors getAndroid() { return baccForAndroidPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.com
         */
        public ComPluginAccessors getCom() { return baccForComPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.kotlin
         */
        public KotlinPluginAccessors getKotlin() { return baccForKotlinPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.org
         */
        public OrgPluginAccessors getOrg() { return baccForOrgPluginAccessors; }

    }

    public static class AndroidPluginAccessors extends PluginFactory  implements PluginNotationSupplier{

        public AndroidPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for android to the plugin id 'android'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("android"); }

            /**
             * Creates a plugin provider for android.library to the plugin id 'android-library'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getLibrary() { return createPlugin("android.library"); }

            /**
             * Creates a plugin provider for android.reporting to the plugin id 'android-reporting'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getReporting() { return createPlugin("android.reporting"); }

    }

    public static class ComPluginAccessors extends PluginFactory {
        private final ComAndroidPluginAccessors baccForComAndroidPluginAccessors = new ComAndroidPluginAccessors(providers, config);
        private final ComDiffplugPluginAccessors baccForComDiffplugPluginAccessors = new ComDiffplugPluginAccessors(providers, config);

        public ComPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at plugins.com.android
         */
        public ComAndroidPluginAccessors getAndroid() { return baccForComAndroidPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.com.diffplug
         */
        public ComDiffplugPluginAccessors getDiffplug() { return baccForComDiffplugPluginAccessors; }

    }

    public static class ComAndroidPluginAccessors extends PluginFactory {
        private final ComAndroidAssetPluginAccessors baccForComAndroidAssetPluginAccessors = new ComAndroidAssetPluginAccessors(providers, config);
        private final ComAndroidDynamicPluginAccessors baccForComAndroidDynamicPluginAccessors = new ComAndroidDynamicPluginAccessors(providers, config);
        private final ComAndroidInternalPluginAccessors baccForComAndroidInternalPluginAccessors = new ComAndroidInternalPluginAccessors(providers, config);

        public ComAndroidPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.application to the plugin id 'com.android.application'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getApplication() { return createPlugin("com.android.application"); }

            /**
             * Creates a plugin provider for com.android.base to the plugin id 'com.android.base'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getBase() { return createPlugin("com.android.base"); }

            /**
             * Creates a plugin provider for com.android.library to the plugin id 'com.android.library'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getLibrary() { return createPlugin("com.android.library"); }

            /**
             * Creates a plugin provider for com.android.lint to the plugin id 'com.android.lint'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getLint() { return createPlugin("com.android.lint"); }

            /**
             * Creates a plugin provider for com.android.reporting to the plugin id 'com.android.reporting'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getReporting() { return createPlugin("com.android.reporting"); }

            /**
             * Creates a plugin provider for com.android.test to the plugin id 'com.android.test'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getTest() { return createPlugin("com.android.test"); }

        /**
         * Returns the group of bundles at plugins.com.android.asset
         */
        public ComAndroidAssetPluginAccessors getAsset() { return baccForComAndroidAssetPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.com.android.dynamic
         */
        public ComAndroidDynamicPluginAccessors getDynamic() { return baccForComAndroidDynamicPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.com.android.internal
         */
        public ComAndroidInternalPluginAccessors getInternal() { return baccForComAndroidInternalPluginAccessors; }

    }

    public static class ComAndroidAssetPluginAccessors extends PluginFactory {
        private final ComAndroidAssetPackPluginAccessors baccForComAndroidAssetPackPluginAccessors = new ComAndroidAssetPackPluginAccessors(providers, config);

        public ComAndroidAssetPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at plugins.com.android.asset.pack
         */
        public ComAndroidAssetPackPluginAccessors getPack() { return baccForComAndroidAssetPackPluginAccessors; }

    }

    public static class ComAndroidAssetPackPluginAccessors extends PluginFactory  implements PluginNotationSupplier{

        public ComAndroidAssetPackPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.asset.pack to the plugin id 'com.android.asset-pack'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("com.android.asset.pack"); }

            /**
             * Creates a plugin provider for com.android.asset.pack.bundle to the plugin id 'com.android.asset-pack-bundle'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getBundle() { return createPlugin("com.android.asset.pack.bundle"); }

    }

    public static class ComAndroidDynamicPluginAccessors extends PluginFactory {

        public ComAndroidDynamicPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.dynamic.feature to the plugin id 'com.android.dynamic-feature'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getFeature() { return createPlugin("com.android.dynamic.feature"); }

    }

    public static class ComAndroidInternalPluginAccessors extends PluginFactory {
        private final ComAndroidInternalAssetPluginAccessors baccForComAndroidInternalAssetPluginAccessors = new ComAndroidInternalAssetPluginAccessors(providers, config);
        private final ComAndroidInternalDynamicPluginAccessors baccForComAndroidInternalDynamicPluginAccessors = new ComAndroidInternalDynamicPluginAccessors(providers, config);
        private final ComAndroidInternalVersionPluginAccessors baccForComAndroidInternalVersionPluginAccessors = new ComAndroidInternalVersionPluginAccessors(providers, config);

        public ComAndroidInternalPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.internal.application to the plugin id 'com.android.internal.application'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getApplication() { return createPlugin("com.android.internal.application"); }

            /**
             * Creates a plugin provider for com.android.internal.library to the plugin id 'com.android.internal.library'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getLibrary() { return createPlugin("com.android.internal.library"); }

            /**
             * Creates a plugin provider for com.android.internal.reporting to the plugin id 'com.android.internal.reporting'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getReporting() { return createPlugin("com.android.internal.reporting"); }

            /**
             * Creates a plugin provider for com.android.internal.test to the plugin id 'com.android.internal.test'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getTest() { return createPlugin("com.android.internal.test"); }

        /**
         * Returns the group of bundles at plugins.com.android.internal.asset
         */
        public ComAndroidInternalAssetPluginAccessors getAsset() { return baccForComAndroidInternalAssetPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.com.android.internal.dynamic
         */
        public ComAndroidInternalDynamicPluginAccessors getDynamic() { return baccForComAndroidInternalDynamicPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.com.android.internal.version
         */
        public ComAndroidInternalVersionPluginAccessors getVersion() { return baccForComAndroidInternalVersionPluginAccessors; }

    }

    public static class ComAndroidInternalAssetPluginAccessors extends PluginFactory {
        private final ComAndroidInternalAssetPackPluginAccessors baccForComAndroidInternalAssetPackPluginAccessors = new ComAndroidInternalAssetPackPluginAccessors(providers, config);

        public ComAndroidInternalAssetPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at plugins.com.android.internal.asset.pack
         */
        public ComAndroidInternalAssetPackPluginAccessors getPack() { return baccForComAndroidInternalAssetPackPluginAccessors; }

    }

    public static class ComAndroidInternalAssetPackPluginAccessors extends PluginFactory  implements PluginNotationSupplier{

        public ComAndroidInternalAssetPackPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.internal.asset.pack to the plugin id 'com.android.internal.asset-pack'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("com.android.internal.asset.pack"); }

            /**
             * Creates a plugin provider for com.android.internal.asset.pack.bundle to the plugin id 'com.android.internal.asset-pack-bundle'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getBundle() { return createPlugin("com.android.internal.asset.pack.bundle"); }

    }

    public static class ComAndroidInternalDynamicPluginAccessors extends PluginFactory {

        public ComAndroidInternalDynamicPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.internal.dynamic.feature to the plugin id 'com.android.internal.dynamic-feature'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getFeature() { return createPlugin("com.android.internal.dynamic.feature"); }

    }

    public static class ComAndroidInternalVersionPluginAccessors extends PluginFactory {

        public ComAndroidInternalVersionPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.android.internal.version.check to the plugin id 'com.android.internal.version-check'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getCheck() { return createPlugin("com.android.internal.version.check"); }

    }

    public static class ComDiffplugPluginAccessors extends PluginFactory {

        public ComDiffplugPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for com.diffplug.spotless to the plugin id 'com.diffplug.spotless'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getSpotless() { return createPlugin("com.diffplug.spotless"); }

    }

    public static class KotlinPluginAccessors extends PluginFactory  implements PluginNotationSupplier{
        private final KotlinAndroidPluginAccessors baccForKotlinAndroidPluginAccessors = new KotlinAndroidPluginAccessors(providers, config);
        private final KotlinDcePluginAccessors baccForKotlinDcePluginAccessors = new KotlinDcePluginAccessors(providers, config);
        private final KotlinNativePluginAccessors baccForKotlinNativePluginAccessors = new KotlinNativePluginAccessors(providers, config);
        private final KotlinPlatformPluginAccessors baccForKotlinPlatformPluginAccessors = new KotlinPlatformPluginAccessors(providers, config);

        public KotlinPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for kotlin to the plugin id 'kotlin'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("kotlin"); }

            /**
             * Creates a plugin provider for kotlin.kapt to the plugin id 'kotlin-kapt'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getKapt() { return createPlugin("kotlin.kapt"); }

            /**
             * Creates a plugin provider for kotlin.multiplatform to the plugin id 'kotlin-multiplatform'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getMultiplatform() { return createPlugin("kotlin.multiplatform"); }

            /**
             * Creates a plugin provider for kotlin.parcelize to the plugin id 'kotlin-parcelize'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getParcelize() { return createPlugin("kotlin.parcelize"); }

            /**
             * Creates a plugin provider for kotlin.scripting to the plugin id 'kotlin-scripting'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getScripting() { return createPlugin("kotlin.scripting"); }

        /**
         * Returns the group of bundles at plugins.kotlin.android
         */
        public KotlinAndroidPluginAccessors getAndroid() { return baccForKotlinAndroidPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.kotlin.dce
         */
        public KotlinDcePluginAccessors getDce() { return baccForKotlinDcePluginAccessors; }

        /**
         * Returns the group of bundles at plugins.kotlin.native
         */
        public KotlinNativePluginAccessors getNative() { return baccForKotlinNativePluginAccessors; }

        /**
         * Returns the group of bundles at plugins.kotlin.platform
         */
        public KotlinPlatformPluginAccessors getPlatform() { return baccForKotlinPlatformPluginAccessors; }

    }

    public static class KotlinAndroidPluginAccessors extends PluginFactory  implements PluginNotationSupplier{

        public KotlinAndroidPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for kotlin.android to the plugin id 'kotlin-android'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("kotlin.android"); }

            /**
             * Creates a plugin provider for kotlin.android.extensions to the plugin id 'kotlin-android-extensions'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getExtensions() { return createPlugin("kotlin.android.extensions"); }

    }

    public static class KotlinDcePluginAccessors extends PluginFactory {

        public KotlinDcePluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for kotlin.dce.js to the plugin id 'kotlin-dce-js'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJs() { return createPlugin("kotlin.dce.js"); }

    }

    public static class KotlinNativePluginAccessors extends PluginFactory {

        public KotlinNativePluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for kotlin.native.cocoapods to the plugin id 'kotlin-native-cocoapods'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getCocoapods() { return createPlugin("kotlin.native.cocoapods"); }

            /**
             * Creates a plugin provider for kotlin.native.performance to the plugin id 'kotlin-native-performance'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getPerformance() { return createPlugin("kotlin.native.performance"); }

    }

    public static class KotlinPlatformPluginAccessors extends PluginFactory {

        public KotlinPlatformPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for kotlin.platform.android to the plugin id 'kotlin-platform-android'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getAndroid() { return createPlugin("kotlin.platform.android"); }

            /**
             * Creates a plugin provider for kotlin.platform.common to the plugin id 'kotlin-platform-common'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getCommon() { return createPlugin("kotlin.platform.common"); }

            /**
             * Creates a plugin provider for kotlin.platform.js to the plugin id 'kotlin-platform-js'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJs() { return createPlugin("kotlin.platform.js"); }

            /**
             * Creates a plugin provider for kotlin.platform.jvm to the plugin id 'kotlin-platform-jvm'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJvm() { return createPlugin("kotlin.platform.jvm"); }

    }

    public static class OrgPluginAccessors extends PluginFactory {
        private final OrgJetbrainsPluginAccessors baccForOrgJetbrainsPluginAccessors = new OrgJetbrainsPluginAccessors(providers, config);

        public OrgPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at plugins.org.jetbrains
         */
        public OrgJetbrainsPluginAccessors getJetbrains() { return baccForOrgJetbrainsPluginAccessors; }

    }

    public static class OrgJetbrainsPluginAccessors extends PluginFactory {
        private final OrgJetbrainsKotlinPluginAccessors baccForOrgJetbrainsKotlinPluginAccessors = new OrgJetbrainsKotlinPluginAccessors(providers, config);

        public OrgJetbrainsPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

        /**
         * Returns the group of bundles at plugins.org.jetbrains.kotlin
         */
        public OrgJetbrainsKotlinPluginAccessors getKotlin() { return baccForOrgJetbrainsKotlinPluginAccessors; }

    }

    public static class OrgJetbrainsKotlinPluginAccessors extends PluginFactory {
        private final OrgJetbrainsKotlinAndroidPluginAccessors baccForOrgJetbrainsKotlinAndroidPluginAccessors = new OrgJetbrainsKotlinAndroidPluginAccessors(providers, config);
        private final OrgJetbrainsKotlinMultiplatformPluginAccessors baccForOrgJetbrainsKotlinMultiplatformPluginAccessors = new OrgJetbrainsKotlinMultiplatformPluginAccessors(providers, config);
        private final OrgJetbrainsKotlinNativePluginAccessors baccForOrgJetbrainsKotlinNativePluginAccessors = new OrgJetbrainsKotlinNativePluginAccessors(providers, config);
        private final OrgJetbrainsKotlinPlatformPluginAccessors baccForOrgJetbrainsKotlinPlatformPluginAccessors = new OrgJetbrainsKotlinPlatformPluginAccessors(providers, config);
        private final OrgJetbrainsKotlinPluginPluginAccessors baccForOrgJetbrainsKotlinPluginPluginAccessors = new OrgJetbrainsKotlinPluginPluginAccessors(providers, config);

        public OrgJetbrainsKotlinPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.js to the plugin id 'org.jetbrains.kotlin.js'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJs() { return createPlugin("org.jetbrains.kotlin.js"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.jvm to the plugin id 'org.jetbrains.kotlin.jvm'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJvm() { return createPlugin("org.jetbrains.kotlin.jvm"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.kapt to the plugin id 'org.jetbrains.kotlin.kapt'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getKapt() { return createPlugin("org.jetbrains.kotlin.kapt"); }

        /**
         * Returns the group of bundles at plugins.org.jetbrains.kotlin.android
         */
        public OrgJetbrainsKotlinAndroidPluginAccessors getAndroid() { return baccForOrgJetbrainsKotlinAndroidPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.org.jetbrains.kotlin.multiplatform
         */
        public OrgJetbrainsKotlinMultiplatformPluginAccessors getMultiplatform() { return baccForOrgJetbrainsKotlinMultiplatformPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.org.jetbrains.kotlin.native
         */
        public OrgJetbrainsKotlinNativePluginAccessors getNative() { return baccForOrgJetbrainsKotlinNativePluginAccessors; }

        /**
         * Returns the group of bundles at plugins.org.jetbrains.kotlin.platform
         */
        public OrgJetbrainsKotlinPlatformPluginAccessors getPlatform() { return baccForOrgJetbrainsKotlinPlatformPluginAccessors; }

        /**
         * Returns the group of bundles at plugins.org.jetbrains.kotlin.plugin
         */
        public OrgJetbrainsKotlinPluginPluginAccessors getPlugin() { return baccForOrgJetbrainsKotlinPluginPluginAccessors; }

    }

    public static class OrgJetbrainsKotlinAndroidPluginAccessors extends PluginFactory  implements PluginNotationSupplier{

        public OrgJetbrainsKotlinAndroidPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.android to the plugin id 'org.jetbrains.kotlin.android'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("org.jetbrains.kotlin.android"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.android.extensions to the plugin id 'org.jetbrains.kotlin.android.extensions'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getExtensions() { return createPlugin("org.jetbrains.kotlin.android.extensions"); }

    }

    public static class OrgJetbrainsKotlinMultiplatformPluginAccessors extends PluginFactory  implements PluginNotationSupplier{

        public OrgJetbrainsKotlinMultiplatformPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.multiplatform to the plugin id 'org.jetbrains.kotlin.multiplatform'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> asProvider() { return createPlugin("org.jetbrains.kotlin.multiplatform"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.multiplatform.pm20 to the plugin id 'org.jetbrains.kotlin.multiplatform.pm20'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getPm20() { return createPlugin("org.jetbrains.kotlin.multiplatform.pm20"); }

    }

    public static class OrgJetbrainsKotlinNativePluginAccessors extends PluginFactory {

        public OrgJetbrainsKotlinNativePluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.native.cocoapods to the plugin id 'org.jetbrains.kotlin.native.cocoapods'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getCocoapods() { return createPlugin("org.jetbrains.kotlin.native.cocoapods"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.native.performance to the plugin id 'org.jetbrains.kotlin.native.performance'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getPerformance() { return createPlugin("org.jetbrains.kotlin.native.performance"); }

    }

    public static class OrgJetbrainsKotlinPlatformPluginAccessors extends PluginFactory {

        public OrgJetbrainsKotlinPlatformPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.platform.android to the plugin id 'org.jetbrains.kotlin.platform.android'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getAndroid() { return createPlugin("org.jetbrains.kotlin.platform.android"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.platform.common to the plugin id 'org.jetbrains.kotlin.platform.common'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getCommon() { return createPlugin("org.jetbrains.kotlin.platform.common"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.platform.js to the plugin id 'org.jetbrains.kotlin.platform.js'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJs() { return createPlugin("org.jetbrains.kotlin.platform.js"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.platform.jvm to the plugin id 'org.jetbrains.kotlin.platform.jvm'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getJvm() { return createPlugin("org.jetbrains.kotlin.platform.jvm"); }

    }

    public static class OrgJetbrainsKotlinPluginPluginAccessors extends PluginFactory {

        public OrgJetbrainsKotlinPluginPluginAccessors(ProviderFactory providers, DefaultVersionCatalog config) { super(providers, config); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.plugin.parcelize to the plugin id 'org.jetbrains.kotlin.plugin.parcelize'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getParcelize() { return createPlugin("org.jetbrains.kotlin.plugin.parcelize"); }

            /**
             * Creates a plugin provider for org.jetbrains.kotlin.plugin.scripting to the plugin id 'org.jetbrains.kotlin.plugin.scripting'
             * This plugin was declared in catalog libs.versions.toml
             */
            public Provider<PluginDependency> getScripting() { return createPlugin("org.jetbrains.kotlin.plugin.scripting"); }

    }

}
