/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.wearable.datalayer

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.wearable.CapabilityClient
import com.google.android.gms.wearable.Node
import com.google.android.gms.wearable.Wearable
import com.google.gson.Gson
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.io.Serializable

class MainActivity : AppCompatActivity() {

    private val messageClient by lazy { Wearable.getMessageClient(this) }
    private val capabilityClient by lazy { Wearable.getCapabilityClient(this) }

    private val clientDataViewModel by viewModels<ClientDataViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MainApp(
                onQueryOtherDevicesClicked = ::onQueryOtherDevicesClicked
            )
        }
    }

    private fun onQueryOtherDevicesClicked() {
        lifecycleScope.launch {
                val nodes = getCapabilitiesForReachableNodes()
            Log.d("Node", Gson().toJson(nodes))
            clientDataViewModel.sendMessage(host = nodes.node,message = nodes.toString(),messageClient = messageClient,payload = Gson().toJson(nodes))


        }
    }

    private suspend fun getCapabilitiesForReachableNodes(): MyNode =
        capabilityClient.getAllCapabilities(CapabilityClient.FILTER_REACHABLE)
            .await().firstNotNullOf {
                MyNode(
                    node = it.value.nodes.first().id,
                    name = it.key
                )
            }
}

data class MyNode(
    val node: String,
    val name: String
):Serializable{
    override fun toString(): String {
        return "{\"node\":\"$node\", \"name\":\"$name\"}"
    }
}